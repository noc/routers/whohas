package subscriber

import (
	"fmt"
)

const (
	LLADDR_PATH    = "/arp-information/ipv4/neighbors/neighbor/state/link-layer-address"
	INTERFACE_PATH = "/arp-information/ipv4/neighbors/neighbor/state/interface-name"
)

type Event struct {
	Name      string
	Timestamp int64
	Values    map[string]string
	Tags      map[string]string
}

type EmptyLLAaddrError struct{}

func (e EmptyLLAaddrError) Error() string {
	return fmt.Sprintf("Update with empty LLAddr value received")
}

type EmptyNeighIPError struct{}

func (e EmptyNeighIPError) Error() string {
	return fmt.Sprintf("Update with empty neighbor_ip tag received")
}
