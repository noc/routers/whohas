package subscriber

type Subscriber interface {
	Subscribe() (chan Event, error)
	Unsubscribe()
}
