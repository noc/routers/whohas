package subscriber

import (
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
)

type NatsSubscriber struct {
	Url          string
	Subject      string
	subscription *nats.Subscription
	msg_chan     chan Event
	conn         *nats.Conn
}

func (n NatsSubscriber) Subscribe() (chan Event, error) {
	opts := nats.Options{
		Url:                  n.Url,
		AllowReconnect:       true,
		MaxReconnect:         10,
		ReconnectWait:        2 * time.Second,
		Timeout:              1 * time.Second,
		RetryOnFailedConnect: true,
	}

	c, err := opts.Connect()
	n.conn = c
	enc, err := nats.NewEncodedConn(c, nats.JSON_ENCODER)
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}
	ch := make(chan Event, 1000)
	n.subscription, _ = enc.BindRecvChan(n.Subject, ch)
	n.msg_chan = ch
	return ch, nil
}

func (n NatsSubscriber) Unsubscribe() {
	fmt.Printf("Unsubing")
	n.subscription.Unsubscribe()
}
