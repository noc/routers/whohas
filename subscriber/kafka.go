package subscriber

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/segmentio/kafka-go"
)

type KafkaSubscriber struct {
	Brokers  []string
	Topic    string
	reader   *kafka.Reader
	msg_chan chan Event
}

func (k KafkaSubscriber) Subscribe() (chan Event, error) {
	d := &kafka.Dialer{
		Timeout: 20 * time.Second,
	}

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:          k.Brokers,
		Dialer:           d,
		Topic:            k.Topic,
		JoinGroupBackoff: 5 * time.Second,
		MaxAttempts:      10,
	})
	fmt.Println("Setting up Kafka connection")
	k.reader = r
	ch := make(chan Event)
	k.msg_chan = ch
	go func() {
		for {
			m, err := r.ReadMessage(context.Background())
			if err != nil {
				fmt.Println(err)
				break
			}
			dec_m := new(Event)
			json.Unmarshal(m.Value, dec_m)
			msg := Event{
				Name:      m.Topic,
				Timestamp: m.Time.Unix(),
				Values:    dec_m.Values,
				Tags:      dec_m.Tags,
			}
			ch <- msg
		}
	}()
	return ch, nil
}

func (k KafkaSubscriber) Unsubscribe() {
	k.reader.Close()
}
