package engine

import (
	"fmt"
	"log/slog"
	"net"
	"whohas/storage"
	"whohas/subscriber"
)

const (
	LLADDR_PATH       = "/arp-information/ipv4/neighbors/neighbor/state/link-layer-address"
	INTERFACE_PATH    = "/arp-information/ipv4/neighbors/neighbor/state/interface-name"
	ND_LLADDR_PATH    = "/nd6-information/ipv6/neighbors/neighbor/state/link-layer-address"
	ND_INTERFACE_PATH = "/nd6-information/ipv6/neighbors/neighbor/state/interface-name"
)

type Engine struct {
	store   storage.Storage
	quit_ch chan struct{}
	logger  *slog.Logger
}

func NewEngine(l *slog.Logger) *Engine {
	return &Engine{
		store:   storage.NewInMemory(),
		quit_ch: make(chan struct{}),
		logger:  l,
	}
}

func (e *Engine) Run(s subscriber.Subscriber) {
	ch, _ := s.Subscribe()
	defer s.Unsubscribe()
	e.logger.Info("Connected to subcriber and starting reading")
	e.readSub(ch)
}

func (e *Engine) readSub(ch chan subscriber.Event) {
	for {
		select {
		case msg := <-ch:
			e.demux(msg)
		case <-e.quit_ch:
			fmt.Println("STOPPING")
			break
		}
	}
}

func (e *Engine) demux(event subscriber.Event) {
	_, isARPMessageMAC := event.Values[LLADDR_PATH]
	_, isNDMessageMAC := event.Values[ND_LLADDR_PATH]
	_, isARPMessageIfce := event.Values[INTERFACE_PATH]
	_, isNDMessageIfce := event.Values[ND_INTERFACE_PATH]

	if isARPMessageMAC {
		e.store.StoreMAC(parseMACEvent(event))
		return
	}
	if isARPMessageIfce {
		e.store.StoreIfce(parseInterfaceEvent(event))
		return
	}
	if isNDMessageMAC {
		e.store.StoreMAC(parseNDMACEvent(event))
		return
	}
	if isNDMessageIfce {
		e.store.StoreIfce(parseNDInterfaceEvent(event))
		return
	}
}

func (e *Engine) Stop() {
	e.quit_ch <- struct{}{}
}

func (e *Engine) ReturnHost(host string) []storage.Row {
	host_info := e.store.Retrieve(host)
	return host_info
}

func (e *Engine) ReturnNodeTable(node string) []storage.Row {
	return e.store.RetrieveNode(node)
}

func parseMACEvent(e subscriber.Event) (ip, mac, node string) {
	ip, _ = e.Tags["neighbor_ip"]
	mac, _ = e.Values[LLADDR_PATH]
	node, _, _ = net.SplitHostPort(e.Tags["source"])
	return node, ip, mac
}

func parseInterfaceEvent(e subscriber.Event) (ip, ifce, node string) {
	ip, _ = e.Tags["neighbor_ip"]
	ifce, _ = e.Values[INTERFACE_PATH]
	node, _, _ = net.SplitHostPort(e.Tags["source"])
	return node, ip, ifce
}

func parseNDMACEvent(e subscriber.Event) (ip, mac, node string) {
	ip, _ = e.Tags["neighbor_ip"]
	mac, _ = e.Values[ND_LLADDR_PATH]
	node, _, _ = net.SplitHostPort(e.Tags["source"])
	return node, ip, mac
}

func parseNDInterfaceEvent(e subscriber.Event) (ip, ifce, node string) {
	ip, _ = e.Tags["neighbor_ip"]
	ifce, _ = e.Values[ND_INTERFACE_PATH]
	node, _, _ = net.SplitHostPort(e.Tags["source"])
	return node, ip, ifce
}
