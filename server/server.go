package server

import (
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	_ "net/http/pprof"
	"whohas/engine"
)

type WhohasServer struct {
	Engine *engine.Engine
	Port   string
	Logger *slog.Logger
	Type   string
}

func (s *WhohasServer) Run() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/host/", s.hostHandler)
	http.HandleFunc("/node/", s.nodeHandler)
	err := http.ListenAndServe(":"+s.Port, nil)
	if err != nil {
		s.Logger.Error("Could not initialize HTTP server")
	}
	s.Logger.Info(fmt.Sprintf("Server started at port %s", s.Port))
}

func indexHandler(w http.ResponseWriter, _ *http.Request) {
	io.WriteString(w, "This is a whohas server, use /host/ and /node endpoints")
}

func (s WhohasServer) hostHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		Hostname string `json:"hostname"`
	}
	var req request
	if r.Body == nil {
		http.Error(w, "Need to specify an IP address", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		fmt.Println(err)
		http.Error(w, "Invalid request data format", 400)
	}
	rows := s.Engine.ReturnHost(req.Hostname)
	json.NewEncoder(w).Encode(rows)
	return
}

func (s WhohasServer) nodeHandler(w http.ResponseWriter, r *http.Request) {
	type request struct {
		Node string `json:"node"`
	}
	var req request
	if r.Body == nil {
		http.Error(w, "Need to specify an IP address", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		fmt.Println(err)
		http.Error(w, "Invalid request data format", 400)
	}
	rows := s.Engine.ReturnNodeTable(req.Node)
	json.NewEncoder(w).Encode(rows)
	return
}
