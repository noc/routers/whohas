# whohas - An ARP table info collector

This project utilizes GNMI capabilities of network devices. GNMIc project and Kafka is used for subscription management and update storing.

This utility consumes updates from a Kafka topic and rebuilds the ARP tables of network devices. Then serves this info to clients via an HTTP server.


