package main

import (
	"whohas/app"
)

func main() {
	app := &app.App{}
	app.Start()
}
