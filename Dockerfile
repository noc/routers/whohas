# Use golang base image
FROM golang:latest AS builder

RUN useradd -ms /bin/sh whohas 
USER whohas

# Set the Current Working Directory inside the container
WORKDIR /home/whohas 

# Copy go mod and sum files
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source code into the container
COPY . .

# Build the Go app
RUN CGO_ENABLED=0 go build -buildvcs=false -v -ldflags='-s -w' -trimpath -o whohas .

FROM scratch

COPY --from=builder /home/whohas/whohas whohas
# Expose port 9090 to the outside world
EXPOSE 9090

# Command to run the executable
CMD ["./whohas"]

