package app

import (
	"log/slog"
	"os"
	"whohas/engine"
	"whohas/server"
	"whohas/subscriber"
)

type Config struct {
	suburl    string //subscriber URL
	subtype   string //subscriber type
	storetype string
	stopic    string //stream topic to subscribe to

	srvtype string
	port    string //local port to bind service to
	loglev  slog.Level
}

type OptFunc func(*Config)

func NewConfig(opts ...OptFunc) *Config {
	c := &Config{
		stopic:    "arp",
		storetype: "inmemory",
		loglev:    slog.LevelInfo,
		srvtype:   "http",
	}
	for _, opt := range opts {
		opt(c)
	}
	return c
}

func WithSubURL() OptFunc {
	url, set := os.LookupEnv("WHOHAS_SUB_URL")
	if !set {
		panic("Need to specify the subscriber url: set env var WHOHAS_SUB_URL")
	}
	return func(c *Config) {
		c.suburl = url
	}
}

func WithSubType() OptFunc {
	st, set := os.LookupEnv("WHOHAS_SUB_TYPE")
	if !set {
		panic("Need to specify the subscriber type: set env var WHOHAS_SUB_TYPE")
	}
	return func(c *Config) {
		c.subtype = st
	}
}

func WithStoreType() OptFunc {
	st, set := os.LookupEnv("WHOHAS_STORAGE_TYPE")
	if !set {
		st = "inmemory"
	}
	return func(c *Config) {
		c.storetype = st
	}
}

func WithSubTopic() OptFunc {
	st, set := os.LookupEnv("WHOHAS_SUB_TOPIC")
	if !set {
		panic("Need to specify the subscriber type: set env var WHOHAS_SUB_TOPIC")
	}
	return func(c *Config) {
		c.stopic = st
	}
}

func WithPort() OptFunc {
	p, set := os.LookupEnv("WHOHAS_PORT")
	if !set {
		p = "9090"
	}
	return func(c *Config) {
		c.port = p
	}
}

func WithLogLevel() OptFunc {
	l, set := os.LookupEnv("WHOHAS_LOGGING")
	if !set {
		l = "INFO"
	}
	return func(c *Config) {
		switch l {
		case "DEBUG":
			c.loglev = slog.LevelDebug
		case "INFO":
			c.loglev = slog.LevelInfo
		}
	}
}

type App struct {
	config *Config
	sub    subscriber.Subscriber
	engine *engine.Engine
	server server.WhohasServer
	logger *slog.Logger
}

func (a *App) Start() {
	c := NewConfig(
		WithPort(),
		WithSubURL(),
		WithSubType(),
		WithStoreType(),
		WithLogLevel(),
	)
	switch c.subtype {
	case "nats":
		a.sub = subscriber.NatsSubscriber{
			Url:     c.suburl,
			Subject: c.stopic,
		}
	case "kafka":
		a.sub = subscriber.KafkaSubscriber{
			Brokers: []string{c.suburl},
			Topic:   c.stopic,
		}
	default:
		panic("No valid subscriber type provided")
	}

	logger := slog.New(slog.NewTextHandler(os.Stdout,
		&slog.HandlerOptions{Level: c.loglev}))

	a.logger = logger

	a.engine = engine.NewEngine(a.logger)
	a.server = server.WhohasServer{
		Port:   c.port,
		Engine: a.engine,
		Logger: a.logger,
		Type:   c.srvtype,
	}
	go a.engine.Run(a.sub)
	a.server.Run()
}
