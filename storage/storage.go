package storage

type Storage interface {
	StoreMAC(string, string, string) error
	StoreIfce(string, string, string) error
	Retrieve(string) []Row
	RetrieveNode(string) []Row
}

type Row struct {
	Node      string
	Hostname  string
	LLAddr    string
	Interface string
}
