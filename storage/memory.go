package storage

import "fmt"

type HostIndex struct {
	hostname   string
	mac_rowid  int
	ifce_rowid int
}

type ARPTable struct {
	node      string
	hosts     map[string]*HostIndex
	mac_rows  []string
	ifce_rows []string
}

func NewARPTable(node string) *ARPTable {
	mac_rows := make([]string, 0)
	ifce_rows := make([]string, 0)
	hosts := make(map[string]*HostIndex)
	return &ARPTable{
		node:      node,
		mac_rows:  mac_rows,
		ifce_rows: ifce_rows,
		hosts:     hosts,
	}
}

func (t *ARPTable) getHostIndex(key string) *HostIndex {
	hi, ok := t.hosts[key]
	if !ok {
		hi := &HostIndex{
			hostname:   key,
			mac_rowid:  -1,
			ifce_rowid: -1,
		}
		t.hosts[key] = hi
		return hi
	}
	return hi
}

func (t *ARPTable) updateMAC(host string, mac string) {
	host_i := t.getHostIndex(host)
	mac_id := host_i.mac_rowid
	switch mac_id {
	case -1:
		t.mac_rows = append(t.mac_rows, mac)
		host_i.mac_rowid = len(t.mac_rows) - 1
	default:
		t.mac_rows[mac_id] = mac
	}
}

func (t *ARPTable) updateIfce(host string, ifce string) {
	host_i := t.getHostIndex(host)
	ifce_id := host_i.ifce_rowid
	switch ifce_id {
	case -1:
		t.ifce_rows = append(t.ifce_rows, ifce)
		host_i.ifce_rowid = len(t.ifce_rows) - 1
	default:
		t.ifce_rows[ifce_id] = ifce
	}
}

func (t *ARPTable) getRow(host string) (Row, bool) {
	host_i, ok := t.hosts[host]
	if !ok {
		return Row{}, false
	}
	return Row{
		Hostname:  host,
		LLAddr:    t.mac_rows[host_i.mac_rowid],
		Interface: t.ifce_rows[host_i.ifce_rowid],
	}, true
}

type InMemory struct {
	nodeIndex map[string]*ARPTable
	quit_ch   chan struct{}
}

func NewInMemory() *InMemory {
	nodeIndex := make(map[string]*ARPTable)
	return &InMemory{
		nodeIndex: nodeIndex,
	}
}

func (m *InMemory) StoreMAC(node, host, mac string) error {
	t, ok := m.nodeIndex[node]
	if !ok {
		t = NewARPTable(node)
		m.nodeIndex[node] = t
	}
	arp_table := t
	arp_table.updateMAC(host, mac)
	return nil
}

func (m *InMemory) StoreIfce(node, host, ifce string) error {
	t, ok := m.nodeIndex[node]
	if !ok {
		t = NewARPTable(node)
		m.nodeIndex[node] = t
	}
	arp_table := t
	arp_table.updateIfce(host, ifce)
	return nil
}

func (m *InMemory) Close() {
	m.quit_ch <- struct{}{}
}

func (m *InMemory) Retrieve(key string) []Row {
	rows := make([]Row, 0)
	for node, table := range m.nodeIndex {
		r, exist := table.getRow(key)
		if !exist {
			continue
		}
		r.Node = node
		rows = append(rows, r)
	}
	return rows
}

func (m *InMemory) RetrieveNode(node string) []Row {
	t, ok := m.nodeIndex[node]
	s := make([]Row, 0)
	if !ok {
		return s
	}
	for host, _ := range t.hosts {
		r, ok := t.getRow(host)
		fmt.Println(r)
		if !ok {
			continue
		}
		s = append(s, r)
	}
	return s
}

func (m *InMemory) RetrieveGroupNode(key string) []Row {
	ret_array := make([]Row, 0)
	nodetable, _ := m.nodeIndex[key]
	for host, host_index := range nodetable.hosts {
		r := Row{
			Node:      key,
			Hostname:  host,
			LLAddr:    nodetable.mac_rows[host_index.mac_rowid],
			Interface: nodetable.ifce_rows[host_index.ifce_rowid],
		}
		ret_array = append(ret_array, r)
	}
	return ret_array
}
