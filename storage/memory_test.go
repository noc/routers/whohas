package storage

import "testing"

func TestRetrieve(t *testing.T) {

	m := NewInMemory()
	m.StoreMAC("test", "192.168.101.1", "00:01:02:03:04:05")
	m.StoreIfce("test", "192.168.101.1", "irb.666")
	m.StoreMAC("testsf", "192.168.101.2", "00:01:02:03:04:06")
	m.StoreIfce("testsf", "192.168.101.2", "irb.667")
	r := m.Retrieve("192.168.101.1")
	if len(r) > 1 {
		t.Error("Got more rows than expected")
	}
	if r[0].Hostname != "192.168.101.1" {
		t.Errorf("got %s, wanted 192.168.101.1", r[0].Hostname)
	}
	r = m.Retrieve("192.168.101.2")
	if len(r) > 1 {
		t.Error("Got more rows than expected")
	}
	if r[0].Hostname != "192.168.101.2" {
		t.Errorf("got %s, wanted 192.168.101.2", r[0].Hostname)
	}
}
